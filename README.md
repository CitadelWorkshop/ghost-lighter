Scripts from a game about a grandma fighting ghosts

Main Title Screen

![MainTitleScreen](https://bitbucket.org/CitadelWorkshop/ghost-lighter/raw/0fb89cd7e02145bc7426185f780907c60405dd50/_ScreenShots/MainTitleScreen.png)

Map Screen

![MapScreen](https://bitbucket.org/CitadelWorkshop/ghost-lighter/raw/0fb89cd7e02145bc7426185f780907c60405dd50/_ScreenShots/MapScreen.png)

Loading Screen

![LoadingScreen](https://bitbucket.org/CitadelWorkshop/ghost-lighter/raw/0fb89cd7e02145bc7426185f780907c60405dd50/_ScreenShots/LoadingScreen.png)

Game Screen 1

![GameScreen1](https://bitbucket.org/CitadelWorkshop/ghost-lighter/raw/0fb89cd7e02145bc7426185f780907c60405dd50/_ScreenShots/GameScreen1.png)

Game Screen 2

![GameScreen2](https://bitbucket.org/CitadelWorkshop/ghost-lighter/raw/0fb89cd7e02145bc7426185f780907c60405dd50/_ScreenShots/GameScreen2.png)

Game Screen 3

![GameScreen3](https://bitbucket.org/CitadelWorkshop/ghost-lighter/raw/0fb89cd7e02145bc7426185f780907c60405dd50/_ScreenShots/GameScreen3.png)
