﻿using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Class for defining events with parameter for event manager
/// </summary>

public class GameEventParam
{
    private static int globalEventId = 0;

    private int eventId;

    public GameEventParam()
    {
        eventId = globalEventId;
        globalEventId += 1;
    }

    public int GetEventId()
    {
        return eventId;
    }
}
