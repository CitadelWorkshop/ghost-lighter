﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Static class for saving game state
/// </summary>

public static class Prefs
{
    private const int MaxHearts = 3;
    private static string level = "level";
    private static string currentUnlockedLevel = "currentUnlockedLevel";

    public static int CurrentUnlockedLevel
    {
        get
        {
            if (PlayerPrefs.HasKey(currentUnlockedLevel))
            {
                return PlayerPrefs.GetInt(currentUnlockedLevel);
            }
            else
            {
                PlayerPrefs.SetInt(currentUnlockedLevel, 0);
                return 0;
            }
        }
        set
        {
            PlayerPrefs.SetInt(currentUnlockedLevel, value);
        }
    }

    public static int GetLevelHearts(int levelNo)
    {
        if (PlayerPrefs.HasKey(level + levelNo))
        {
            return PlayerPrefs.GetInt(level + levelNo);
        }
        else
        {
            PlayerPrefs.SetInt(level + levelNo, 0);
            return 0;
        }
    }

    public static void SetLevelHearts(int levelNo, int heartCount)
    {
        if (heartCount > 3) heartCount = MaxHearts;
        PlayerPrefs.SetInt(level + levelNo, heartCount);
    }

}
