﻿using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Class for defining events for event manager
/// </summary>

public class GameEvent
{
    private static int globalEventId = 0;

    private int eventId;

    public GameEvent()
    {
        eventId = globalEventId;
        globalEventId += 1;
    }

    public int GetEventId()
    {
        return eventId;
    }

}
