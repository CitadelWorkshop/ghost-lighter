﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Singleton that manage basic game behaviours and states
/// </summary>

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    public float lanternRotateSpeed = 5f;
    public float floorSpeed         = 0.8f;

    //Lantern Stats 
    public int maxHealth = 3;
    public float spotlightAngle = 60f;
    public float beamLightAngle = 9.5f;
    public float lightDamage = 1f;
    public float fastLightReloadingSpeed = 15;
    public float slowLightReloadingSpeed = 0;
    public float maxBeamLightTime = 4;
    public float pushForce = 5;
    public float pushForceArea = 1f;

    //Level
    public int levelNumber = 0; //current level number
    public float levelTime = 30f;  //in secs

    public List<Level> levels = new List<Level>();

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex != 2) return; //only works for level scene
        if(levelNumber == 0) //first level always has a 3 hearts
        {
            maxHealth = 3; 
        }
        else
        {
            //get hearts from previous levels
            maxHealth = Prefs.GetLevelHearts(levelNumber-1); 
        }
        if (maxHealth == 0) maxHealth = 3; //if there is no pref for this level than assume player is playing it first time
        EventManager.TriggerEvent(EventManager.LevelLoaded, levels[levelNumber]); //send with event level scriptable object with data about it
    }

    private void ReloadLevel()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene);
        AudioManager.instance.Stop("PlayerBanished");
    }
}
