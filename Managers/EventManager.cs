﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Manager allows objects to subscribe, unsubscribe and trigger defined events without and with parameter 
/// </summary>

public static class EventManager 
{
    public static GameEvent LevelStart = new GameEvent();
    public static GameEvent LevelEnd = new GameEvent();
    public static GameEvent PlayerKilled = new GameEvent();
    public static GameEvent LevelReturnToMap = new GameEvent();
    public static GameEvent NeedReload = new GameEvent();

    public static GameEventParam LevelLoaded     = new GameEventParam();

    private static Dictionary<int,Action> eventDictionary = new Dictionary<int, Action>();
    private static Dictionary<int, Action<object>> eventParamDictionary = new Dictionary<int, Action<object>>();

    public static void StartListening(GameEvent gameEvent, Action listener)
    {
        Action thisEvent;

        if (eventDictionary.TryGetValue(gameEvent.GetEventId(), out thisEvent))
        {
            thisEvent += listener;
            eventDictionary[gameEvent.GetEventId()] = thisEvent;
        }
        else
        {
            thisEvent += listener;
            eventDictionary.Add(gameEvent.GetEventId(), thisEvent);
        }
    }

    public static void StopListening(GameEvent gameEvent, Action listener)
    {
        Action thisEvent;

        if (eventDictionary.TryGetValue(gameEvent.GetEventId(), out thisEvent))
        {
            thisEvent -= listener;
            eventDictionary[gameEvent.GetEventId()] = thisEvent;
        }
    }

    public static void TriggerEvent(GameEvent gameEvent)
    {
        Action thisEvent = null;
        if (eventDictionary.TryGetValue(gameEvent.GetEventId(), out thisEvent))
        {
            thisEvent();
        }
    }

    public static void TriggerEvent(GameEventParam gameEvent, object param)
    {
        Action<object> thisEvent = null;
        if (eventParamDictionary.TryGetValue(gameEvent.GetEventId(), out thisEvent))
        {
            thisEvent(param);
        }
    }

    public static void StartListening(GameEventParam gameEvent, Action<object> listener)
    {
        Action<object> thisEvent;

        if (eventParamDictionary.TryGetValue(gameEvent.GetEventId(), out thisEvent))
        {
            thisEvent += listener;
            eventParamDictionary[gameEvent.GetEventId()] = thisEvent;
        }
        else
        {
            thisEvent += listener;
            eventParamDictionary.Add(gameEvent.GetEventId(), thisEvent);
        }
    }

    public static void StopListening(GameEventParam gameEvent, Action<object> listener)
    {
        Action<object> thisEvent;

        if (eventParamDictionary.TryGetValue(gameEvent.GetEventId(), out thisEvent))
        {
            thisEvent -= listener;
            eventParamDictionary[gameEvent.GetEventId()] = thisEvent;
        }
    }
}
