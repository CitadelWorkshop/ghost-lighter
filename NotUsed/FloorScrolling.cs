﻿using UnityEngine;

public class FloorScrolling : MonoBehaviour
{

    public float distanceMoved = 0f;
    public FloorScrolling floor1;
    public FloorScrolling floor2;
    public SpriteRenderer floorRenderer;


    private float verticalLength;
    private Rigidbody2D floorBody;
    private bool startScrolling = false;
    
    private const float levelDistance = 100f;

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelLoaded, OnLevelLoaded);
        EventManager.StartListening(EventManager.LevelStart, OnLevelStart);
        EventManager.StartListening(EventManager.PlayerKilled, OnPlayerKilled);
    }

    public void OnLevelLoaded(object obj)
    {
        Level level = obj as Level;
      //  floorRenderer.sprite = level.floorSprite;
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelLoaded, OnLevelLoaded);
        EventManager.StopListening(EventManager.LevelStart, OnLevelStart);
        EventManager.StopListening(EventManager.PlayerKilled, OnPlayerKilled);
    }

    public void OnLevelStart()
    {
        startScrolling = true;
    }

   /* public void LoadFloorSprite(Level level) TODO
    {
        floorRenderer.sprite = level.floorSprite;
    }
    */

    public void OnPlayerKilled() //TODO
    {
        gameObject.SetActive(false);
    }

    void Start ()
    {
        floorBody = GetComponent<Rigidbody2D>();
        verticalLength = floorRenderer.bounds.size.y;
        floorBody.velocity = Vector2.down * GameManager.instance.floorSpeed;
        distanceMoved = levelDistance;
    }
	
	void Update ()
    {
        if(transform.position.y < - verticalLength)
        {
            Vector2 floorOffset = new Vector2(0f, verticalLength * 2f);
            transform.position = (Vector2)transform.position + floorOffset;
        }

        if (!startScrolling) return;

        distanceMoved -= Time.deltaTime * (levelDistance / GameManager.instance.levelTime); //determine level ends 

        if (distanceMoved <= 0)
        {
            distanceMoved = 0;

            EventManager.TriggerEvent(EventManager.LevelEnd);
            floor1.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            floor2.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            floor1.enabled = false;
            floor2.enabled = false;
        }
    }
}
