﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


public class DestinationMover : MonoBehaviour
{
    public LevelLoader loader;
    public Transform[] waypoints;
    public Fader fader;

    public float moveTime = 5f; //move time to destination in seconds

    private Transform currentDestination;
    private SpriteRenderer figureRenderer;
    private int currentLevel = 1;
    private bool isCoroutineActive;

    private void Start()
    {
        figureRenderer = GetComponent<SpriteRenderer>();
        transform.position = waypoints[0].transform.position;
        fader.StartFadeIn(null, 1.5f);
    }

    public bool GetIsCoroutineActive()
    {
        return isCoroutineActive;
    }

    public IEnumerator GoToLevelDestination(int levelNo, Vector3 levelPosition)
    {
        isCoroutineActive = true;

        float relativeSpeed;
        float distance = 0;
        //check level waypoint index
        int levelWaypointIndex = 0;
        int currentPositionIndex = 0;

        bool goingUp = false;

        for(int i = 0; i < waypoints.Length; i++)
        {
            if(levelPosition == waypoints[i].position)
            {
                levelWaypointIndex = i;
                break;
            }
        }

        for(int i = 0; i < waypoints.Length; i++)
        {
            if (transform.position == waypoints[i].position)
            {
                currentPositionIndex = i;
                break;
            }
        }

        if (levelWaypointIndex > currentPositionIndex) //idziemy do wyzszego levelu
        {
            for (int i = currentPositionIndex; i < levelWaypointIndex; i++)
            {
                distance += Vector2.Distance(waypoints[i].position, waypoints[i + 1].position);
            }
            goingUp = true;
        }
        else if (levelWaypointIndex < currentPositionIndex) //idziemy do nizszego levelu
        {
            for (int i = levelWaypointIndex; i < currentPositionIndex; i++)
            {
                distance += Vector2.Distance(waypoints[i].position, waypoints[i + 1].position);
            }
            goingUp = false;
        }

        relativeSpeed = distance / moveTime;

        //sprawdz czy sprite powinien byc odwrocony TODO

        while(transform.position != waypoints[levelWaypointIndex].position)
        {
            currentPositionIndex = goingUp ? currentPositionIndex + 1 : currentPositionIndex - 1;

            figureRenderer.flipX = (waypoints[currentPositionIndex].position.x > transform.position.x) ? false : true;

            while (transform.position != waypoints[currentPositionIndex].position)
            {
                transform.position = Vector2.MoveTowards(transform.position, waypoints[currentPositionIndex].position, relativeSpeed * Time.deltaTime);
                yield return null;
            }
            yield return null;
        }

        fader.StartFadeOut(FadeIn, 1f);
    }


    private void FadeIn()
    {
        loader.gameObject.SetActive(true);
        fader.StartFadeIn(Load, 1f);
    }

    private void Load()
    {
        loader.LoadLevel();
    }

}
