﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelection : MonoBehaviour
{
    public LevelLoader loader;
    public Transform[] levelPositions;
    public Fader fader;

    private void Start()
    {
        fader.StartFadeIn(null, 1.5f);
    }

    public IEnumerator GoToLevelDestination(int levelNo, Vector3 levelPosition)
    {
        GameManager.instance.levelNumber = levelNo;
        transform.position = levelPositions[levelNo].transform.position;
        yield return new WaitForSeconds(1);
        fader.StartFadeOut(FadeIn, 1f);
        yield return null;
    }

    private void FadeIn()
    {
        loader.gameObject.SetActive(true);
        fader.StartFadeIn(loader.LoadLevel, 1f);
    }



}
