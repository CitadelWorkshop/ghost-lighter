﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Toggle ghosts canBeHit flag when entering or exiting the game area
/// </summary>

public class GhostHitableArea : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ghost")
        {
            Ghost ghost = collision.GetComponent<Ghost>();
            ghost.ToggleGhostCanBeHitFlag(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "Ghost")
        {
            Ghost ghost = collision.GetComponent<Ghost>();
            ghost.ToggleGhostCanBeHitFlag(false);
        }
    }
}
