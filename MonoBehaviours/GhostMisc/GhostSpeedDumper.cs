﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Slow down the ghost when they approach from sides to balance the game
/// </summary>

public class GhostSpeedDumper : MonoBehaviour
{
    public float speedDumperPercent = 30f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ghost")
        {
            Ghost ghost = collision.GetComponent<Ghost>();

            float velocity = ghost.GetVelocity();
            ghost.SetVelocity(velocity * speedDumperPercent / 100f);
        }
    }
}
