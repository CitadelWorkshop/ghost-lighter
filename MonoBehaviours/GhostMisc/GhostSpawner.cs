﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Creates ghost pool using data from level scriptable object that contains random types of ghosts.
/// After every seconds attempts to spawn ghost if there is available ghost in ghost pool
/// Ghost count that spawner use is increment over time
/// Special ghost is spawned after certain amount of time
/// </summary>

public class GhostSpawner : MonoBehaviour
{
    public float initCircleRadius = 1f;
    public float ringRadius = 5f;
 
    private int secTimer = 0;
    private int secTimer2 = 0;
    private int availableGhostsCount = 1; //start with one ghost available 
    private int ghostPoolSize;
    private int specialGhostSpawnTimer;
    private float addGhostTimer;
    private const float spawnDistance = 10f;
    private List<Ghost> basicGhostPool;
    private List<Ghost> ghostTypes;
    private Ghost specialGhost;

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelLoaded, OnLevelLoaded);
        EventManager.StartListening(EventManager.LevelEnd, OnLevelEnd);
        EventManager.StartListening(EventManager.LevelStart, OnLevelStart);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelLoaded, OnLevelLoaded);
        EventManager.StopListening(EventManager.LevelEnd, OnLevelEnd);
        EventManager.StopListening(EventManager.LevelStart, OnLevelStart);
    }

    private void OnLevelStart()
    {
        StartCoroutine(SpawnLogic());
    }

    private void OnLevelLoaded(object param)
    {
        Level level = param as Level;
        ghostTypes = new List<Ghost>(level.ghosts);
        specialGhost = level.specialGhost;
        addGhostTimer = level.addGhostTimer;
        ghostPoolSize = level.ghostPoolSize;
        specialGhostSpawnTimer = level.specialGhostSpawnTimer;
        PrepereGhostPool();
        PrepereSpecialGhost();
    }

    private void OnLevelEnd()
    {
        CancelInvoke("SpawnLogic");
        gameObject.SetActive(false);
    }

    private void PrepereGhostPool()
    {
        basicGhostPool = new List<Ghost>();
        for(int i = 0; i < ghostPoolSize; i++)
        {
            int randomGhostIndex = Random.Range(0, ghostTypes.Count);
            Ghost ghostObject = Instantiate(ghostTypes[randomGhostIndex],new Vector3(0,ringRadius,0),Quaternion.identity);
            ghostObject.gameObject.SetActive(false);
            basicGhostPool.Add(ghostObject);
        }
    }

    private void PrepereSpecialGhost()
    {
        if (specialGhost == null) return;
        specialGhost = Instantiate(specialGhost, new Vector3(0, ringRadius, 0), Quaternion.identity);
        specialGhost.gameObject.SetActive(false);
    }

    IEnumerator SpawnLogic()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            SpawnRandomGhost();
            
            secTimer++;
            if(secTimer >= addGhostTimer)
            {
                secTimer = 0;
                availableGhostsCount++;
                if (availableGhostsCount > ghostPoolSize)
                {
                    availableGhostsCount = ghostPoolSize;
                }
            }

            secTimer2++;
            if(secTimer2 >= specialGhostSpawnTimer)
            {
                secTimer2 = 0;
                SpawnSpecialGhost();
            }
        }
    }

    private void SpawnSpecialGhost()
    {
        if (specialGhost == null) return;
        if(!specialGhost.gameObject.activeInHierarchy)
        {
            Vector2 pos = CalculateSpawnGhostPosition();
            specialGhost.transform.position = pos;
            specialGhost.gameObject.SetActive(true);
        }
    }

    private void SpawnRandomGhost()
    {
        Vector2 pos = Vector2.zero;

        for(int i = 0; i < availableGhostsCount; i++)
        {
            if(!basicGhostPool[i].gameObject.activeInHierarchy)
            {
                pos = CalculateSpawnGhostPosition();
                 
                Ghost ghostToSpawn = basicGhostPool[i];
                ghostToSpawn.transform.position = pos;
                SpriteRenderer ghostRenderer = ghostToSpawn.GetComponent<SpriteRenderer>();

                if (pos.x > 0)
                {
                    ghostRenderer.flipX = true;
                }
                else
                {
                    ghostRenderer.flipX = false;
                }
                ghostToSpawn.gameObject.SetActive(true);
                break;
            }
        }
    }

    private Vector2 CalculateSpawnGhostPosition()
    {
        // calculeting random position in the circle with radius 10
        Vector2 pos = UnityEngine.Random.insideUnitCircle * initCircleRadius;

        //change to polar coordinate system
        float angleRadian = Mathf.Atan2(pos.x, pos.y);
        float R = Mathf.Sqrt(Vector2.SqrMagnitude(pos));

        //add ring radius
        R = R + ringRadius;

        //convert to Cartesian system
        pos = new Vector2(Mathf.Cos(angleRadian) * R, Mathf.Sin(angleRadian) * R);

        return pos;
    }
}
