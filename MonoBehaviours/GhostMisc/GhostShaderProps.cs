﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets ghost visiblity via shader properties in shader script
/// </summary>

[ExecuteInEditMode]
public class GhostShaderProps : MonoBehaviour
{
    public float  allVisionTime = 1f;
    public float angleOffset;
    public string posPropertyName;
    public string rotPropertyName;
    public string angPropertyName;
    public Material mat;
    public Light spotLight;

    private bool allVisionFlag = false;
    private float currentAllVisionTime;
	
	void Update ()
    {
        if(allVisionFlag)
        {
            if(currentAllVisionTime < allVisionTime)
            {
                mat.SetFloat(angPropertyName, 360f + angleOffset);
                currentAllVisionTime += Time.deltaTime;
            }
            else
            {
                currentAllVisionTime = 0;
                allVisionFlag = false;
            }
        }
        else
        {
            mat.SetVector(posPropertyName, transform.position);
            mat.SetVector(rotPropertyName, -transform.forward);
            mat.SetFloat(angPropertyName, spotLight.spotAngle + angleOffset);   
        }
	}

    public void SetAllVisionFlag()
    {
        allVisionFlag = true;
    }
}
