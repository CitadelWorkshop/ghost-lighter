﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// Manages level loading behaviour
/// </summary>

public class LevelLoader : MonoBehaviour
{
    public float levelLoadDelay = 0;
    public float faderDelay = 1f;
    public Fader fader;
    private AsyncOperation operation;

    public void LoadLevel()
    {
        StartCoroutine(LoadAsynchronously(SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void LoadLevelWithFadeOut()
    {
        StartCoroutine(LoadAsynchronouslyFadeOut(SceneManager.GetActiveScene().buildIndex + 1));
    }


    public void LoadLevel(int levelIndex)
    {
        StartCoroutine(LoadAsynchronously(levelIndex));
    }

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        operation = SceneManager.LoadSceneAsync(sceneIndex);

        while(!operation.isDone)
        {
            yield return null;
        }
    }

    IEnumerator LoadAsynchronouslyFadeOut(int sceneIndex)
    {
        float timer = 0;
        operation = SceneManager.LoadSceneAsync(sceneIndex);
        operation.allowSceneActivation = false;

        while (!operation.isDone && timer < levelLoadDelay)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        if(fader != null)
        {
            fader.StartFadeOut(LoadAfterFadeOut, faderDelay);
        }
    }

    private void LoadAfterFadeOut()
    {
        operation.allowSceneActivation = true;
    }
}
