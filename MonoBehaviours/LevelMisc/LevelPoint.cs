﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Loads clicked level with a blink indicator
/// Manages level image color and hearts
/// </summary>

public class LevelPoint : MonoBehaviour
{
    public int levelNo;
    public SpriteRenderer[] hearts;
    public SpriteRenderer clickArea;

    private int heartsCount;
    private float blinkTime = 0.1f;
    private GameObject heartBar;
    private SpriteRenderer levelRenderer;
    private LevelController levelController;
    private PolygonCollider2D levelCollider;
    private Color blueColor;

    private void Start()
    {
        levelController = GetComponentInParent<LevelController>();

        levelRenderer = GetComponent<SpriteRenderer>();

        levelCollider = GetComponent<PolygonCollider2D>();

        ColorUtility.TryParseHtmlString("#92C5DFFF", out blueColor);

        heartsCount = Prefs.GetLevelHearts(levelNo);

        if (heartsCount == 0 && levelNo != Prefs.CurrentUnlockedLevel)
        {
            LevelDisable();
        }
        else
        {
            AssignHearts(heartsCount);
        }
    }

    private void OnMouseDown()
    {
        StartCoroutine(LevelSelect());
    }

    private void AssignHearts(int heartCount)
    {
        for(int i = 0; i < hearts.Length; i++) //clear all hearts
        {
            hearts[i].sprite = levelController.emptyHeart;
        }

        for(int i = 0; i < heartCount; i++) //assign hearts
        {
            hearts[i].sprite = levelController.fullHeart;
        }
    }

    private void LevelDisable()
    {
        levelCollider.enabled = false;
        levelRenderer.color = Color.gray;
        clickArea.color = Color.gray;

        for (int i = 0; i < hearts.Length; i++) //clear all hearts
        {
            hearts[i].sprite = levelController.emptyHeart;
        }
    }

    private IEnumerator LevelSelect()
    {
        float time = 0;

        for(int i = 0; i < 3; i++)
        {
            time = 0;

            while (time < blinkTime)
            {
                time += Time.deltaTime;
                clickArea.color = Color.Lerp(Color.white, blueColor, time / blinkTime);
                yield return null;
            }

            time = 0;

            while (time < blinkTime)
            {
                time += Time.deltaTime;
                clickArea.color = Color.Lerp(blueColor, Color.white, time / blinkTime);
                yield return null;
            }
        }
        levelController.OnLevelPointerDown(levelNo);
    }
}
