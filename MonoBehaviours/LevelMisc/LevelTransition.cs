﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Controls level transitions
/// </summary>

public class LevelTransition : MonoBehaviour
{
    public Fader fader;
    public LanternHealth lanternHealth;

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        fader.StartFadeIn(null, 1.5f);
    }

    private void OnLevelReturnToMap()
    {
        fader.StartFadeOut(ReturnToMap, 1.5f);
    }

    private void ReturnToMap()
    {
        if(lanternHealth.GetCurrentHealh() == 0) //if dead , just return to map screen
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
            return;
        }
    
        int currentLevel = GameManager.instance.levelNumber;
        int currentLevelSavedHearts = Prefs.GetLevelHearts(currentLevel);

        if(currentLevelSavedHearts != 0) //this level was played before
        {
            if (lanternHealth.GetCurrentHealh() > Prefs.GetLevelHearts(currentLevel)) //if there is more hearts from the playing than set it permanently
            {
                Prefs.SetLevelHearts(currentLevel, lanternHealth.GetCurrentHealh());
            }
        }
        else //this level was not played before
        {
            Prefs.CurrentUnlockedLevel += 1;
            Prefs.SetLevelHearts(currentLevel, lanternHealth.GetCurrentHealh());
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
        EventManager.StartListening(EventManager.LevelReturnToMap, OnLevelReturnToMap);
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
        EventManager.StopListening(EventManager.LevelReturnToMap, OnLevelReturnToMap);
    }
}
