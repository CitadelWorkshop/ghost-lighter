﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Plays current level theme
/// </summary>

public class LevelMusicPlayer : MonoBehaviour
{
    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelLoaded, OnLevelLoaded);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelLoaded, OnLevelLoaded);
    }

    private void OnLevelLoaded(object param)
    {
        Level level = param as Level;
        AudioManager.instance.StopAll();
        AudioManager.instance.Play(level.levelMusicName);
    }
}
