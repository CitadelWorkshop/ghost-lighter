﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manage level loading and fading animation
/// Passes hearts sprites to levelPoint
/// </summary>

public class LevelController : MonoBehaviour
{
    public LevelLoader levelLoader;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    private Fader fader;

    private void Start() 
    {
        fader = GetComponent<Fader>();
        fader.StartFadeIn(null, 1.5f);
    }

    public void LoadLevel(int levelNo)
    {
        Debug.Log(levelNo);
        GameManager.instance.levelNumber = levelNo;
        fader.StartFadeOut(FadeIn, 1f);
    }

    private void FadeIn()
    {
        levelLoader.gameObject.SetActive(true);
        fader.StartFadeIn(levelLoader.LoadLevelWithFadeOut, 1f);
    }

    public void OnLevelPointerDown(int levelNo)
    {
        LoadLevel(levelNo);
    }
}
