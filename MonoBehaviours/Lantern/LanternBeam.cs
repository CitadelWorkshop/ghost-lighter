﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manage beam of light that hurts enemies
/// </summary>

public class LanternBeam : MonoBehaviour
{
    [HideInInspector] public bool lanternBeamToggle;
    [HideInInspector] public bool lanternBeamBlock;

    public LanternHealth lanternHealth;
    public Light spotlight;
    public SpriteRenderer headRenderer;
    public Sprite headLight;
    public Sprite headDark;
    public Animator perfectReloadAnimation;

    private bool audioIsPlying = false;
    private float pushForce;
    private float currentBeamLightTime;
    private Animator beamAnimator;
    private BoxCollider2D beamLightCollider;
    private List<Ghost> ghostsInsideBeam = new List<Ghost>();
    private Light beamLight;
    private GhostShaderProps shaderProperty;

    private const float maxLightBeamIntensity = 200f;
    private const float maxSpotLightIntensity = 100f;

    void Start ()
    {
        beamLight = GetComponent<Light>();
        beamLightCollider = GetComponent<BoxCollider2D>();
        beamAnimator = GetComponent<Animator>();
        shaderProperty = GetComponent<GhostShaderProps>(); 
        currentBeamLightTime = GameManager.instance.maxBeamLightTime;
        spotlight.spotAngle = GameManager.instance.spotlightAngle;
        beamLight.spotAngle = GameManager.instance.beamLightAngle;
        pushForce = GameManager.instance.pushForce;
        beamLight.intensity = maxLightBeamIntensity;
        spotlight.intensity = maxSpotLightIntensity;
        lanternBeamBlock = false;
    }

    void Update()
    {
        spotlight.intensity = maxSpotLightIntensity; //reseting spotlight intensity after reloading
        if (lanternBeamBlock)
        {
            beamAnimator.enabled = true;
            beamAnimator.SetBool("lampBroken", true);
            return;
        }
        else
        {
            beamAnimator.SetBool("lampBroken", false);
            beamAnimator.enabled = false;
        }

        if (lanternBeamToggle)
        {
            headRenderer.sprite = headLight;
            beamLightCollider.enabled = true;
            beamLight.intensity = maxLightBeamIntensity;

            if (!audioIsPlying)
            {
                audioIsPlying = true;
                AudioManager.instance.Play("LanternBeam");
            }

            currentBeamLightTime -= Time.deltaTime;

            if (currentBeamLightTime < 0f)
            {
                lanternBeamToggle = false;
                lanternBeamBlock = true;
                EventManager.TriggerEvent(EventManager.NeedReload);
                AudioManager.instance.Play("LanternBroken");
                StopBeamLight();
            }
        }
        else
        {
            headRenderer.sprite = headDark;
            StopBeamLight();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Ghost")
        {
            Ghost ghostInside = collision.GetComponent<Ghost>();
            ghostInside.ToggleGhostHitFlag(true);
            ghostsInsideBeam.Add(ghostInside);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ghost")
        {
            Ghost ghostInside = collision.GetComponent<Ghost>();
            ghostInside.ToggleGhostHitFlag(false);
        }
    }

    private void StopBeamLight()
    {
        AudioManager.instance.Stop("LanternBeam");
        audioIsPlying = false;
        beamLightCollider.enabled = false;
        beamLight.intensity = 0f;
        StopHittingGhosts();
    }

    private void StopHittingGhosts()
    {
        if (ghostsInsideBeam.Count != 0)
        {
            foreach (Ghost ghost in ghostsInsideBeam)
            {
                ghost.ToggleGhostHitFlag(false);
            }

            ghostsInsideBeam.Clear();
        }
    }

    public void ReloadLantern(float speed)
    {
        currentBeamLightTime += Time.deltaTime * speed;
        if (currentBeamLightTime > GameManager.instance.maxBeamLightTime)
        {
            currentBeamLightTime = GameManager.instance.maxBeamLightTime;
            lanternBeamBlock = false;
        }
    }

    public void PerfectReloadForcePush()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position, GameManager.instance.pushForceArea);
        for(int i = 0; i < colliders.Length; i++)
        {
            if(colliders[i].tag != "Ghost")
            {
                continue;
            }
            Ghost ghost = colliders[i].GetComponent<Ghost>();
         
            Vector2 forceDirection = -1 * (transform.position - ghost.transform.position).normalized;
            ghost.SetDisableMovement(true);
            ghost.GetGhostBody().velocity = Vector2.zero;
            ghost.GetGhostBody().AddForce(forceDirection * pushForce,ForceMode2D.Impulse); //push ghost away from the player
        }
        perfectReloadAnimation.SetTrigger("ReloadPush");
        shaderProperty.SetAllVisionFlag(); //set flag for SetShaderProperty to see all the ghosts
    }

    public float GetCurrentBeamLightTime()
    {
        return currentBeamLightTime;
    }

}
