﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Turn player according to the circle pad and control alarm vibration movement
/// </summary>

public class LanternMovement : MonoBehaviour
{
    public float    shakeMagnitude = 1.5f;
    public LanternAnimation lanternAnimation;

    private float   lanternAngle;
    private Vector2 initPosition;

    private void Start()
    {
        initPosition = transform.position; 
    }

    void Update ()
    {
        transform.localEulerAngles = Vector3.back * lanternAngle;
    }

    //Circle Pad sets lantern angle;
    public void SetLanternAngle(float angle)
    {
        lanternAngle = angle;
    }

    public float GetLanternAngle()
    {
        return transform.localEulerAngles.z;
    }

    public void Shake(float shakeMag)
    {
        lanternAnimation.DisableLanternAnimator();
        transform.position = (Vector3)UnityEngine.Random.insideUnitCircle * shakeMag * Time.deltaTime;
    }

    public void StopShake()
    {
        lanternAnimation.EnableLanternAnimator();
        transform.position = initPosition;
    }

}
