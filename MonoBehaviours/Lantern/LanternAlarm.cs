﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Turn on player vibration when ghosts are in alarm zone and player is not seeing them
/// Vibration is manage by lanterMovement script
/// </summary>

public class LanternAlarm : MonoBehaviour
{
    public  float shakeMagnitude = 1.5f;
    private LanternMovement lanternMovement;
    private Ghost ghostInsideAlarm = null;
    private List<Ghost> allGhostsInside = new List<Ghost>();

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelEnd, OnLevelEnd);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelEnd, OnLevelEnd);
    }

    private void OnLevelEnd()
    {
        gameObject.SetActive(false);
    }

    private void Start()
    {
        lanternMovement = GetComponentInParent<LanternMovement>();
    }

    void Update ()
    {
        //if there is no ghost inside List turn off the alarm
        if (allGhostsInside.Count == 0) return;

        //remove ghosts from List if they are null (destoyed by lantern) or check if they are in range of the spotlight
        for(int i = 0; i < allGhostsInside.Count; i++) 
        {
            Ghost ghost = allGhostsInside[i];

            //if ghost is unactive and cannot be hit remove current ghost from the list
            if (!ghost.gameObject.activeInHierarchy || !ghost.GetIsGhostHittable()) 
            {
                allGhostsInside.Remove(ghost);
                if (allGhostsInside.Count == 0)
                {
                    lanternMovement.StopShake();
                    return;
                }
            }
            else 
            {
                //use more than half of the angle otherwise alarm is not turning off even ghost is almost visible
                float spotlightAngle = GameManager.instance.spotlightAngle * 0.6f;
                float delta = CalculateGhostAngle();

                if (delta > spotlightAngle)
                {
                    lanternMovement.Shake(shakeMagnitude);
                    break;
                }
                else
                {
                    lanternMovement.StopShake();
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ghost")
        {
            ghostInsideAlarm = collision.GetComponent<Ghost>();
            ghostInsideAlarm.OnTriggerAlarmEnter();
            allGhostsInside.Add(ghostInsideAlarm);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Ghost")
        {
            for (int i = 0; i < allGhostsInside.Count; i++)
            {
                if (ReferenceEquals(allGhostsInside[i], collision.GetComponent<Ghost>()))
                {
                    allGhostsInside.Remove(allGhostsInside[i]);
                }
            }
        }
    }

    //Calculate ghost angle between 0 - 360 degrees and return shortest angle between lantern and ghost
    private float CalculateGhostAngle()
    {
        Vector2 lanternPos = lanternMovement.transform.InverseTransformDirection(Vector2.up);
        Vector2 realtiveVector = Vector2.up - lanternPos;
        Vector2 ghostPos = ghostInsideAlarm.transform.position;

        float lanternAngle = Mathf.Atan2(realtiveVector.y, realtiveVector.x) * Mathf.Rad2Deg * 2f;
        float dot = Vector2.Dot(ghostPos, Vector2.up);
        float det = Vector2.up.x * ghostPos.y - Vector2.up.y * ghostPos.x;
        float ghostAngle = -Mathf.Atan2(det, dot) * Mathf.Rad2Deg;

        if (ghostAngle < 0) ghostAngle += 360f;
        float delta = Mathf.Abs(Mathf.DeltaAngle(lanternAngle, ghostAngle));
        return delta;
    }

}
