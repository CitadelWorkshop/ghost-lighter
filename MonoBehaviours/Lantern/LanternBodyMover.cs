﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Player's body rotates only when head rotation exceed tilt angle
/// </summary>
/// 
public class LanternBodyMover : MonoBehaviour
{
    public float            tilt = 0;
    public LanternMovement  lanternMovement;

	void Update ()
    {
        float lanternAngle = lanternMovement.GetLanternAngle();
        float bodyAngle = transform.localEulerAngles.z;
        float anglesDiff = Mathf.DeltaAngle(bodyAngle, lanternAngle);

        if (anglesDiff > tilt )
        {
            transform.localEulerAngles = Vector3.forward * (lanternAngle - tilt);
        }
          
        if (anglesDiff < -tilt)
        {
            transform.localEulerAngles = Vector3.forward * (lanternAngle  + tilt);
        }
    }
}
