﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Show reload indicator when OnNeedReload event accur and return indicator size to LanternBeam script
/// </summary>

public class ReloadIndicator : MonoBehaviour
{
    public float            maxReloadIndicatorScale = 1;
    public float            maxIndicatorSpeed = 0.2f;
    public float            indicatorAcceleration = 0.025f;
    public SpriteRenderer   lanternRenderer; //powinno byc cos zwiaznego z reloadingiem w sensie nazwa

    private float           reloadIndicatorScale = 1;
    private float           currentIndicatorSpeed;
    private SpriteRenderer  reloadIndicatorRenderer;

    void Start ()
    {
        reloadIndicatorRenderer = GetComponent<SpriteRenderer>();
        reloadIndicatorRenderer.enabled = false;
        reloadIndicatorScale    = maxReloadIndicatorScale;
        currentIndicatorSpeed = maxIndicatorSpeed;
        lanternRenderer.enabled = false;
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.NeedReload, OnNeedReload);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.NeedReload, OnNeedReload);
    }

    void OnNeedReload()
    {
        lanternRenderer.enabled = true;
        StartCoroutine("ShowReloadIndicator");
    }

    public void HideReloadIndicator()
    {
        lanternRenderer.enabled = false;
        StopCoroutine("ShowReloadIndicator");
        currentIndicatorSpeed = maxIndicatorSpeed;
        reloadIndicatorScale = maxReloadIndicatorScale;
        reloadIndicatorRenderer.enabled = false;
    }

    public float GetIndicatorScale()
    {
        return reloadIndicatorScale;
    }

    private IEnumerator ShowReloadIndicator()
    {
        while(reloadIndicatorScale > 0)
        {
            reloadIndicatorRenderer.enabled = true;
            reloadIndicatorRenderer.transform.localScale = new Vector3(reloadIndicatorScale, reloadIndicatorScale, 0);
            reloadIndicatorScale -= Time.deltaTime * currentIndicatorSpeed;
            currentIndicatorSpeed += indicatorAcceleration;
            yield return null;
        }
    }
}
