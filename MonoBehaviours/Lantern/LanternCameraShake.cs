﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Shake camera when player is hit by ghost
/// </summary>

public class LanternCameraShake : MonoBehaviour
{
    public float shakeMagnitude;
    public  Camera gameCamera;

    private Vector3 initPos;
  
	void Start ()
    {
        initPos = gameCamera.transform.position;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ghost")
        {  
            InvokeRepeating("Shake", 0, .01f); //shake camera in 10ms inverval
            Invoke("StopShaking", 0.5f); //after 500ms stop shaking
        }
    }

    private void Shake()
    {
        Vector3 randomPos = Random.insideUnitCircle * shakeMagnitude * Time.deltaTime;
        transform.position += randomPos;
    }

    private void StopShaking()
    {
        CancelInvoke("Shake");
        gameCamera.transform.position = initPos;
    }

}
