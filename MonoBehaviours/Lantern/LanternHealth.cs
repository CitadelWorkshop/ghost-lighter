﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manage player's healh, flash screen when damage is taken, trigger OnPlayerDead event
/// </summary>

public class LanternHealth : MonoBehaviour
{
    public int currentHealth;
    public float flashSpeed = 2f;
    public Image damageImage;

    private int maxHealth;
    private Color damageColor = new Color(0f, 0f, 0f, 0.9f);

    private void Start()
    {
        maxHealth = GameManager.instance.maxHealth;
        currentHealth = maxHealth;
    }

    public void DamageLantern()
    {
        DamageTaken();
        AudioManager.instance.Play("HitByGhost");

        if (currentHealth <= 0)
        {
            PlayerDead();
            return;
        }
    }

    private void DamageTaken()
    {
        currentHealth -= 1;
        if (damageImage == null) return;
        damageImage.color = damageColor;
        StartCoroutine(FlashDamageImage());
    }

    private void PlayerDead() 
    {
        EventManager.TriggerEvent(EventManager.LevelReturnToMap);
    }

    public int GetCurrentHealh()
    {
        return currentHealth;
    }

    IEnumerator FlashDamageImage()
    {
        while (!damageImage.color.Equals(Color.clear))
        {
            damageImage.color = Color.Lerp(damageImage.color, Color.clear, Time.deltaTime * flashSpeed);
            yield return null;
        }
    }
}
