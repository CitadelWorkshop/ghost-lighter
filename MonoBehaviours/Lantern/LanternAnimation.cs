﻿using UnityEngine;
using System;

/// <summary>
/// Control animations in game states, and trigger start and end of the level
/// </summary>

public class LanternAnimation : MonoBehaviour
{
    private Animator lanternAnimator;

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelEnd, OnLevelEnd);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelEnd, OnLevelEnd);
    }

    private void Start ()
    {
        lanternAnimator = GetComponent<Animator>();
    }

    private void OnLevelEnd()
    { 
        EnableLanternAnimator();
        lanternAnimator.SetTrigger("LampLeaveTheLevel");
    }

    public void OnAnimatorLeaveTheLevel()
    {
        EventManager.TriggerEvent(EventManager.LevelReturnToMap);
    }

    public void OnAnimatorEventTriggerStart()
    {
        EventManager.TriggerEvent(EventManager.LevelStart);
    }

    //used to play sounds in animations
    public void OnAnimationEventPlaySound()
    {
        AudioManager.instance.Play("LanternBroken");
    }

    public void DisableLanternAnimator()
    {
        lanternAnimator.enabled = false;
    }

    public void EnableLanternAnimator()
    {
        lanternAnimator.enabled = true;
    }

}
