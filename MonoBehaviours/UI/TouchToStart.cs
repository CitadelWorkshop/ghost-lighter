﻿using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// After animation, touch the screen to load map screen
/// </summary>

public class TouchToStart : MonoBehaviour, IPointerDownHandler
{
    private LevelLoader levelLoader;
    private Fader fader;

    private void Start()
    {
        levelLoader = GetComponent<LevelLoader>();
        fader = GetComponent<Fader>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        fader.StartFadeOut(LoadGame, 1.5f);
    }

    public void LoadGame()
    {
        levelLoader.LoadLevel();
    }
}
