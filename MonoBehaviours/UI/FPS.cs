﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPS : MonoBehaviour
{
    private Text fpsText;
    int frameCount = 0;
    double dt = 0f;
    double fps = 0f;
    double updateRate = 4f;  // 4 updates per sec.

    private void Start()
    {
        fpsText = GetComponent<Text>();
    }

    void Update ()
    {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0 / updateRate)
        {
            fps = frameCount / dt;
            frameCount = 0;
            dt -= 1.0 / updateRate;
            fpsText.text = "FPS: " + ((int)fps).ToString();
        }
       
	}
}
