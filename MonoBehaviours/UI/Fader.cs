﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Fade out and fade in animation that can run passed functions at the end
/// </summary>

public class Fader : MonoBehaviour
{
    public RawImage blackImage;

    public void StartFadeIn(Action func, float time)
    {
        StartCoroutine(FadeIn(func, time));
    }

    public void StartFadeOut(Action func, float time)
    {
        StartCoroutine(FadeOut(func, time));
    }

    private IEnumerator FadeOut(Action func, float time)
    {
        float step = 0;
        float alpha = 0f;

        for (step = 0.0f; step < 1.0f; step += Time.deltaTime / time)
        {
            Color newColor = new Color(0, 0, 0, Mathf.Lerp(alpha, 1, step));
            blackImage.color = newColor;
            yield return null;
        }

        if (func != null) func();
    }

    private IEnumerator FadeIn(Action func, float time)
    {
        float step = 0;
        float alpha = 1f;

        for (step = 0.0f; step < 1.0f; step += Time.deltaTime / time)
        {
            Color newColor = new Color(0, 0, 0, Mathf.Lerp(alpha, 0, step));
            blackImage.color = newColor;
            yield return null;
        }

        if (func != null) func();
    }
}
