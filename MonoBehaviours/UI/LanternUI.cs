﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Controls lantern UI 
/// </summary>

public class LanternUI: MonoBehaviour
{
    public Text distanceText;
    public Sprite fullHeart;
    public Sprite emptyHeart;
    public Image[] heartImages;
    public FloorsScrolling floor;
    public Slider lightSlider;
    public LanternHealth lanternHealth;
    public LanternBeam lanternBeam;

    private int lastFullHearsCount;

    private void Start()
    {
        for (int i = 0; i < GameManager.instance.maxHealth; i++)
        {
            heartImages[i].sprite = fullHeart;
        }

        lastFullHearsCount = GameManager.instance.maxHealth;
        lightSlider.maxValue = GameManager.instance.maxBeamLightTime;
    }

    void LateUpdate ()
    {
        distanceText.text   = "Distance " + ((int)floor.distanceMoved).ToString();
 
        lightSlider.value = lanternBeam.GetCurrentBeamLightTime();

        if (lanternHealth.currentHealth != lastFullHearsCount)
        {
            Debug.Log("current health = " + lanternHealth.currentHealth + "\n" + "last health = " + lastFullHearsCount);
            heartImages[lanternHealth.currentHealth].sprite = emptyHeart;
            lastFullHearsCount = lanternHealth.currentHealth;
        }
	}
}
