﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Enable/disable light beam for kiling ghosts or manage reloading behaviour
/// </summary>

public class SwitchPad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public LanternBeam lanternBeam;
    public Sprite[] onOffSprite;
    public ReloadIndicator reloadIndicator;

    private bool needToReload = false;
    private float reloadingSpeed;
    private Image switchImage;

    private void Start()
    {
        switchImage = GetComponent<Image>();
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.NeedReload, OnNeedReload);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.NeedReload, OnNeedReload);
    }

    private void OnNeedReload()
    {
        needToReload = true;
    }

    private void Update()
    {
        if (lanternBeam.lanternBeamBlock)
        {
            switchImage.sprite = onOffSprite[0];
            lanternBeam.lanternBeamToggle = false;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (needToReload)
        {
            needToReload = false;
            float reloadScaleTime = reloadIndicator.GetIndicatorScale();
            if (reloadScaleTime >= 0.37 && reloadScaleTime <= 0.53) //for this setup 0.45 is perfect
            {
                reloadingSpeed = GameManager.instance.fastLightReloadingSpeed;
                AudioManager.instance.Play("PerfectReload");
                AudioManager.instance.Play("ReloadingShort");
                lanternBeam.PerfectReloadForcePush();
            }
            else
            {
                reloadingSpeed = GameManager.instance.slowLightReloadingSpeed;
                AudioManager.instance.Play("ReloadingLong");
            }
            reloadIndicator.HideReloadIndicator();
            StartCoroutine("Reloading");
            return;
        }

        if (!lanternBeam.lanternBeamBlock)
        {
            AudioManager.instance.Play("SwitchPadOn");
            switchImage.sprite = onOffSprite[1];
            lanternBeam.lanternBeamToggle = true;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        AudioManager.instance.Play("SwitchPadOff");
        switchImage.sprite = onOffSprite[0];
        lanternBeam.lanternBeamToggle = false;
    }

    IEnumerator Reloading()
    {
        while (lanternBeam.lanternBeamBlock)
        {
            lanternBeam.ReloadLantern(reloadingSpeed);
            yield return null;
        }
    }
}
