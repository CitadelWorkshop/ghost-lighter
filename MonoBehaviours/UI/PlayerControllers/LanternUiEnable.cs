﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Show and hide lantern ui depends on the level events
/// </summary>

public class LanternUiEnable : MonoBehaviour
{
    public GameObject circlePad;
    public GameObject switchPad;
    public GameObject lanternUI;

    private Animator playerUiAnimator;

    private void Awake()
    {
        playerUiAnimator = GetComponent<Animator>();
        playerUiAnimator.enabled = false;
        circlePad.SetActive(false);
        switchPad.SetActive(false);
        lanternUI.SetActive(false);
    }

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelStart, ShowPlayerUI);
        EventManager.StartListening(EventManager.LevelEnd, HidePlayerUI);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelStart, ShowPlayerUI);
        EventManager.StopListening(EventManager.LevelEnd, HidePlayerUI);
    }

    public void ShowPlayerUI()
    {
        playerUiAnimator.enabled = true;
        circlePad.SetActive(true);
        switchPad.SetActive(true);
        lanternUI.SetActive(true);
    }

    public void HidePlayerUI()
    {
        playerUiAnimator.SetTrigger("PlayerUiFadeOut");
    }
}
