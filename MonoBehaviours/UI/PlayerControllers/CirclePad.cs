﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

/// <summary>
/// Rotating circle pad rotates player character
/// </summary>

public class CirclePad : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    public bool test = false; //enable test controls
    public float testSpeed = 1f; //speed of test controls

    public float clickDistanceFromCenter = 30f;
    public LanternMovement lanternMovement;

    private float currentAngle = 0f;
    private float targetAngle = 0f;
    private float angle = 0f;
    private RectTransform rectT;

    private void Start()
    {
        rectT = GetComponent<RectTransform>();
    }

    private void Update()
    {
        if(test) //test controller
        {
            if(Input.GetKey(KeyCode.A))
            {
                angle -= testSpeed;
            }
            else if(Input.GetKey(KeyCode.D))
            {
                angle += testSpeed;
            }
            lanternMovement.SetLanternAngle(angle);
            return;
        }

        //calculate circle pad angle and pass it to lanternMovement script
        angle = Mathf.LerpAngle(currentAngle, targetAngle, GameManager.instance.lanternRotateSpeed * Time.deltaTime);
        rectT.localEulerAngles = Vector3.back * angle;
        currentAngle = angle;

        lanternMovement.SetLanternAngle(angle);
    }

    public void OnDrag(PointerEventData eventData)
    {
        float distance = Vector2.Distance(transform.position, eventData.position);
        //be sure that you are at the rim of the criclePad, clicking inside do nothing
        if (distance < clickDistanceFromCenter)
        {
            return;
        }

        targetAngle = -1f * Vector2.SignedAngle(Vector2.up, eventData.position - new Vector2(rectT.position.x, rectT.position.y));
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        float distance = Vector2.Distance(transform.position, eventData.position);
        //be sure that you are at the rim of the criclePad, clicking inside do nothing
        if (distance < clickDistanceFromCenter)
        {
            return;
        }
        targetAngle = -1f * Vector2.SignedAngle(Vector2.up, eventData.position - new Vector2(rectT.position.x, rectT.position.y));
    }
}
