﻿using UnityEngine;

/// <summary>
/// Plays music for start screen
/// </summary>

public class StartScreenAudioPlayer : MonoBehaviour
{
    private void Start()
    {
        AudioManager.instance.Play("StartTheme");
    }
}
