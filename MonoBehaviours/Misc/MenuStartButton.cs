﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public GameObject levelLoader;
    private LevelLoader levelLoaderScript;

    private void Awake()
    {
        levelLoaderScript = levelLoader.GetComponent<LevelLoader>();
        levelLoader.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoadGame()
    {
        levelLoader.SetActive(true);
        levelLoaderScript.LoadLevel();
    }
}
