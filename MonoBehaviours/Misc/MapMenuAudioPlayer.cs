﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMenuAudioPlayer : MonoBehaviour
{
	void Start ()
    {
        AudioManager.instance.StopAll();
        AudioManager.instance.Play("MapTheme"); 
	}
}
