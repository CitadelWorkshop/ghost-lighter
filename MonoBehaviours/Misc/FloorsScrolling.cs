﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scrolls floor tiles and trigger level ends event
/// </summary>

public class FloorsScrolling : MonoBehaviour
{
    public int numberOfFloorTiles = 8;
    public float distanceMoved = 0f;
    public SpriteRenderer floorRenderer;

    private Rigidbody2D floorBody;
    private float verticalLength;
    private const float levelDistance = 100f;

    private void OnEnable()
    {
        EventManager.StartListening(EventManager.LevelStart, OnLevelStart);
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelStart, OnLevelStart);
    }

    void Start ()
    {
        floorBody = GetComponent<Rigidbody2D>();
        distanceMoved = levelDistance;
    }
	
	void Update ()
    {
        distanceMoved -= Time.deltaTime * (levelDistance / GameManager.instance.levelTime); //determine level ends 

        if (distanceMoved <= 0)
        {
            distanceMoved = 0;

            EventManager.TriggerEvent(EventManager.LevelEnd);
            floorBody.velocity = Vector2.zero;
        }
    }

    public void OnLevelStart()
    {
        verticalLength = floorRenderer.bounds.size.y * numberOfFloorTiles;
        float floorSpeed = verticalLength / GameManager.instance.levelTime;
        floorBody.velocity = Vector2.down * floorSpeed;
    }
}
