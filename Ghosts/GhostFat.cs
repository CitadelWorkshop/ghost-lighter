﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ghost that is slow and tanky but his movement can be stopped by lighting on him
/// </summary>

public class GhostFat : Ghost
{
    public override void MoveTowardsPlayer()
    {
        if (player == null) return;

        if (isHit)
        {
            currentGhostVelocity = 0.05f;
        }
        else
        {
            currentGhostVelocity = 0.2f;
        }

        Vector2 direction = (player.transform.position - transform.position).normalized;
        ghostBody.MovePosition(new Vector2(transform.position.x, transform.position.y) + direction * currentGhostVelocity * Time.deltaTime);
    }
}







