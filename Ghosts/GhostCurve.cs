﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ghost move towards player at curve
/// </summary>

public class GhostCurve : Ghost
{
    private int turnDirection;

    protected void Start()
    {
        turnDirection = Random.Range(0, 2) * 2 - 1;
    }

    public override void MoveTowardsPlayer()
    {
        if (player == null) return;

        Vector2 direction = (player.transform.position - transform.position).normalized;
        Vector2 perpendicularVector = new Vector2(-direction.y, direction.x);
        Vector2 evadeMove = (perpendicularVector * turnDirection) * currentGhostVelocity * Time.deltaTime;
        Vector2 forwardMove = direction * currentGhostVelocity * Time.deltaTime;

        ghostBody.MovePosition(ghostBody.position + evadeMove + forwardMove);
    }
}
