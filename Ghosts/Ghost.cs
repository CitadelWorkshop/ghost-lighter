﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/// <summary>
/// Base class for Ghosts
/// </summary>


public abstract class Ghost : MonoBehaviour
{
    public float ghostVelocity = 0.25f;
    public float timeToDie = 1f;
    public float disableMovementTime = 1f;
    public Slider healthBar;
    public Material material;
    public Material visibleMaterial;

    protected bool isHit = false;
    protected bool isDying = false;
    protected bool canBeHit = false;
    protected bool disableMovement = false;
    protected float currentGhostVelocity;
    protected float currentTime;
    protected float colliderSize;
    protected Image healthImage;
    protected Animator ghostAnimator;
    protected GameObject player;
    protected LanternHealth lanternHealth;
    protected Rigidbody2D ghostBody;
    protected BoxCollider2D ghostCollider;
    protected MaterialPropertyBlock propertyBlock;
    protected SpriteRenderer ghostRenderer;

    private float timer;
    private string matDistortionPropertyName = "_DistortToggle";

    public abstract void MoveTowardsPlayer();

    protected virtual void OnEnable() //reseting ghost state
    {
        EventManager.StartListening(EventManager.LevelEnd, OnLevelEnd);
        ghostCollider.enabled = true;
        healthBar.maxValue = timeToDie;
        currentGhostVelocity = ghostVelocity;
        currentTime = timeToDie;
        healthImage.enabled = false;
        isHit = false;
        isDying = false;
        canBeHit = false;
        ghostAnimator.enabled = true;
    }

    private void OnDisable()
    {
        EventManager.StopListening(EventManager.LevelEnd, OnLevelEnd);
    }

    protected virtual void Awake ()
    {
        ghostCollider = gameObject.GetComponent<BoxCollider2D>();
        ghostBody = GetComponent<Rigidbody2D>();
        ghostAnimator = GetComponent<Animator>();
        ghostRenderer = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Lantern");
        lanternHealth = player.GetComponentInChildren<LanternHealth>();
        healthBar.maxValue = timeToDie;
        currentGhostVelocity = ghostVelocity;
        currentTime = timeToDie;
        healthImage = healthBar.GetComponentInChildren<Image>();
        healthImage.enabled = false;
        propertyBlock = new MaterialPropertyBlock();
    }

    protected virtual void Update()
    {
        if(isHit)
        {
            ToggleDistortion(1);
            healthImage.enabled = true;
            TakingDamge();   
        }
        else
        {
            ToggleDistortion(0);
            healthImage.enabled = false;
        }
    }

    protected void FixedUpdate()
    {
        //if ghost is pushed, disable its movement to get it pushed for certain amount of time
        if (disableMovement) 
        {
            ghostAnimator.SetBool("GhostPushed", true);
            if (timer < disableMovementTime)
            { 
                timer += Time.deltaTime;
            }
            else
            {
                disableMovement = false;
                timer = 0;
                ghostAnimator.SetBool("GhostPushed", false);
            }
            return;
        }

        if (!isDying)
        {
            MoveTowardsPlayer();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //stop the ghost, make him visible, unkillable and start attack animation
        if (collision.tag == "Lantern")
        {
            SetVelocity(0);
            ToggleGhostCanBeHitFlag(false);
            ghostRenderer.material = visibleMaterial;
            ghostAnimator.SetTrigger("GhostAttack");
        }
    }

    public void OnTriggerAlarmEnter()
    {
        //when ghost is in lantern alarm trigger zone change animation
        ghostAnimator.SetTrigger("GhostPrepToAttack");
    }

    public void OnAnimationGhostAttack()
    {
        lanternHealth.DamageLantern();

        //disable ghost after attack and restore his certain states
        gameObject.SetActive(false);
        ghostAnimator.enabled = false;
        ghostRenderer.material = material;
    }

    public virtual void OnAnimationGhostKilled()   
    {
        //used in animation event when ghost is dead
        gameObject.SetActive(false);
        ghostAnimator.enabled = false ;
    }

    public void OnLevelEnd()
    {
        gameObject.SetActive(false);
        ghostAnimator.enabled = false;
    }

    public void TakingDamge()
    {
        currentTime -= Time.deltaTime * GameManager.instance.lightDamage;
       
        if(healthBar != null)
        {
            healthBar.value = currentTime;
        }

        if (currentTime < 0 && !isDying && canBeHit)
        {
            isDying = true;
            ghostBody.velocity = Vector2.zero;
            ToggleGhostCanBeHitFlag(false);
            ghostAnimator.SetTrigger("GhostDead");
            AudioManager.instance.Play("GhostDying");
        }
    }

    public virtual void ToggleGhostHitFlag(bool state) 
    {
        if (!canBeHit)
        {
            AudioManager.instance.Stop("GhostFrying");
            return;
        }

        if(state)
        {
            AudioManager.instance.Play("GhostFrying");
        }
        else
        {
            AudioManager.instance.Stop("GhostFrying");
        }

        isHit = state;
    }

    public void ToggleGhostCanBeHitFlag(bool state) 
    {
        //when ghost is allowed to be hit by the player
        canBeHit = state;
    }

    protected void ToggleDistortion(int val) 
    {
        //sprite distortion via shader
        ghostRenderer.GetPropertyBlock(propertyBlock);
        propertyBlock.SetFloat(matDistortionPropertyName, val);
        ghostRenderer.SetPropertyBlock(propertyBlock);
    } 

    public void SetVelocity(float velocity)
    {
        currentGhostVelocity = velocity;
    }

    public void SetDisableMovement(bool toggle)
    {
        disableMovement = toggle;
    }

    public bool GetIsGhostHittable()
    {
        return canBeHit;
    }

    public float GetVelocity()
    {
        return currentGhostVelocity;
    }

    public Rigidbody2D GetGhostBody()
    {
        return ghostBody;
    }
}



