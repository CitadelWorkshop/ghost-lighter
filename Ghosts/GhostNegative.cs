﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ghost that change his color in intervals, lightin on him damage him or healing him depending on his color
/// </summary>

public class GhostNegative : Ghost
{
    private bool colorToggle;
    private float time = 1f;

    protected void Start()
    {
        InvokeRepeating("ColorChange", time, time);
    }

    protected override void Update()
    {
        if(isHit)
        {
            if(!colorToggle)
            {
                base.TakingDamge();
            }
            else
            {
                GhostHeal();
            }
        }
    }

    public override void MoveTowardsPlayer()
    {
        if (player == null) return;
        Vector2 direction = (player.transform.position - transform.position).normalized;
        ghostBody.MovePosition(new Vector2(transform.position.x, transform.position.y) + direction * currentGhostVelocity * Time.deltaTime);
    }

    private void GhostHeal()
    {
        currentTime += Time.deltaTime * GameManager.instance.lightDamage;

        if (healthBar != null)
        {
            healthBar.value = currentTime;
        }

        if (currentTime > timeToDie)
        {
            currentTime = timeToDie;
        }
    }

    private void ColorChange()
    {
        colorToggle = !colorToggle;
    }
}