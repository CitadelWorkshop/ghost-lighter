﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ghost moving at player with a evade motion
/// </summary>

public class GhostEvade : Ghost
{
    public int random; //used for randomize sinus phase that is depend on game global time
    public float evadeVelocity = 1f;
    public float evadeMag = 2f;
    public float evadeFreq = 0.3f;

    protected void Start()
    {
        random = Random.Range(1, 5);
    }

    public override void MoveTowardsPlayer()
    {
        if (player == null) return;

        Vector2 direction = (player.transform.position - transform.position).normalized;
        Vector2 perpendicularVector = new Vector2(-direction.y, direction.x);
        Vector2 evadeMove = perpendicularVector * evadeVelocity * Time.deltaTime;
        Vector2 forwardMove = direction * currentGhostVelocity * Time.deltaTime;

        evadeVelocity = Mathf.Sin(2 * Mathf.PI * evadeFreq * Time.time * (1+(random * 0.1f))) * evadeMag;
        ghostBody.MovePosition(ghostBody.position + evadeMove + forwardMove);
    }
}
