﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ghost that run faster after lighting on him
/// </summary>

public class GhostRunner : Ghost
{
    private bool firstHit = false;
    public float speedMultiplayer = 3;

    protected override void OnEnable()
    {
        base.OnEnable();
        firstHit = false;
    }

    public override void MoveTowardsPlayer()
    {
        if (player == null) return;

        if(isHit || firstHit)
        {
            firstHit = true;
            currentGhostVelocity += speedMultiplayer * Time.deltaTime;
        }

        Vector2 direction = (player.transform.position - transform.position).normalized;
        ghostBody.MovePosition(new Vector2(transform.position.x, transform.position.y) + direction * currentGhostVelocity * Time.deltaTime);
    }

    protected override void Update()
    {
        base.Update();
        if(disableMovement)
        {
            currentGhostVelocity = 0;
        }
    }
}
