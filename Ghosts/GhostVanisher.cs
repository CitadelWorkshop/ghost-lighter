﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ghost that can teleport himself several times after getting damage from player
/// </summary>

public class GhostVanisher : Ghost
{
    public float teleportAngle = 50;
    public float teleportCount = 2;

    private float currentTeleportCount;

    private bool wasTeleported = false;

    protected override void OnEnable()
    {
        base.OnEnable();
        currentTeleportCount = teleportCount;
        wasTeleported = false;
    }

    protected override void Update()
    {
        if(isHit)
        {
            if(wasTeleported)
            {
                TakingDamge();
            }
            else
            {
                ghostAnimator.SetTrigger("GhostTeleport");
                isHit = false; 
            }
        }
    }

    public override void MoveTowardsPlayer()
    {
        if (player == null) return;
        Vector2 direction = (player.transform.position - transform.position).normalized;
        ghostBody.MovePosition(new Vector2(transform.position.x, transform.position.y) + direction * currentGhostVelocity * Time.deltaTime);
    }

    public override void ToggleGhostHitFlag(bool state)
    {
        isHit = state;
        if (isHit && wasTeleported)
        {
            AudioManager.instance.Play("GhostFrying");
        }
        else
        {
            AudioManager.instance.Stop("GhostFrying");
        }
    }

    private void Teleport()
    {
        int side = Random.Range(0, 1);
      
        Vector2 teleportDest = transform.position; 
         //change to polar coordinate system
         float angle = Mathf.Deg2Rad * teleportAngle;
         float angleRadian = Mathf.Atan2(transform.position.y, transform.position.x);
         float R = Mathf.Sqrt(Vector2.SqrMagnitude(transform.position));
      
         if(side == 0)
         {
           angleRadian -= angle;
         }
         else
         {
           angleRadian += angle;
         }

        //convert to Cartesian system
        teleportDest = new Vector2(Mathf.Cos(angleRadian) * R, Mathf.Sin(angleRadian) * R);
        transform.position = teleportDest;

        currentTeleportCount--;
        isHit = false;
        if(currentTeleportCount == 0)
        {
            wasTeleported = true;
        }
    }












}
