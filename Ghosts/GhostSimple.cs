﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic Ghost
/// </summary>

public class GhostSimple : Ghost
{
    public override void MoveTowardsPlayer()
    {
        if (player == null) return;
        Vector2 direction = (player.transform.position - transform.position).normalized;
        ghostBody.MovePosition(new Vector2(transform.position.x, transform.position.y) + direction * currentGhostVelocity * Time.deltaTime);
    }
}
