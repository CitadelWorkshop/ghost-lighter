﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Level")]
public class Level : ScriptableObject
{
    public string levelName;
    public int ghostPoolSize;
    public int addGhostTimer;
    public int specialGhostSpawnTimer;
    public List<Ghost> ghosts;
    public Ghost specialGhost;
    public string levelMusicName;
}
