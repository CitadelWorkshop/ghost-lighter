﻿  Shader "Custom/GhostShader" 
  {
    Properties 
	{
		_MainTex ("Texture", 2D) = "white" {}
		_NoiseTex ("Texture", 2D) = "white" {}
		_LightDirection("Light Direction", Vector) = (0,0,1,0)
		_LightPosition("Light Position", Vector) = (0,0,0,0)
		_LightAngle("Light Angle", Float) = 45
		_DistortionValue("Distortion Value", Range(0,0.1)) = 0.03
		_DistortionFrequency("Distortion Frequency", Float) = 10
		[PerRendererData] _DistortToggle("Distort Toggle", Float) = 0
    }
   
    SubShader 
	{
		Tags
        {         
			"Queue"="Transparent" 
			"RenderType" = "Transparent"
			"CanUseSpriteAtlas"="True"
			"ForceNoShadowCasting" = "True"
			"IgnoreProjector"="True"
        }

		Cull Off //for sprite flip
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass
		{
			CGPROGRAM

			#pragma vertex vertexFunc
			#pragma fragment fragmentFunc
			
			
			struct appdata 
			{
				float4 vertex	:POSITION;
				float2 uv		:TEXCOORD0;
				float3 worldPos :TEXCOORD1;
			};
	  
			struct v2f
			{
	  			float4 position   : SV_POSITION;
				float2 uv		  : TEXCOORD0;
				float3 worldPos   : TEXCOORD1;
			};

			sampler2D _MainTex;
			sampler2D _NoiseTex;
			fixed4 _Color;
			float4 _LightPosition;
			float4 _LightDirection;
			float _LightAngle;
			float _DistortionValue;
			float _DistortionFrequency;
			int _DistortToggle;
			//vertex Shader

			v2f vertexFunc(appdata IN)
			{
				v2f OUT;

				OUT.position = UnityObjectToClipPos(IN.vertex);
				OUT.worldPos = mul(unity_ObjectToWorld, IN.vertex).xyz;
				OUT.uv = IN.uv;

				return OUT;
			}

			//fragment

			fixed4 fragmentFunc(v2f IN) : SV_TARGET
			{
		
				float distort = tex2D(_NoiseTex,IN.uv).r * (1 + sin(_Time.y * _DistortionFrequency)*0.1);

				distort -= 0.5;

				distort *= _DistortToggle;

				fixed4 textureColor = tex2D(_MainTex,IN.uv + (distort * _DistortionValue) );

				float3 direction = normalize(_LightPosition - IN.worldPos); //get direction between ghost and lantern
				float scale = dot(direction, _LightDirection); //if ghost at front return 1, if perpendicular return 0
				float strength = scale - cos(_LightAngle * 0.5 *  0.01745329); //PI/180  -> change to radians
				strength = min(max(strength * 50, 0), 1);
				textureColor = float4(textureColor.r,textureColor.g,textureColor.b,textureColor.a * strength);

				return textureColor;
			}
		
			ENDCG
		}
    }
    Fallback "Diffuse"
  }